# NMigen Util 

This project implements utilities for nmigen.  It is semi-equivalent
to Chisel3 io library.  Its development is covered under an NLNet Grant
and the top-level bugreport is here:
http://bugs.libre-soc.org/show_bug.cgi?id=62

Documentation: https://libre-soc.org/docs

# Requirements

* nmigen
* yosys (latest git repository, required by nmigen)
* pyvcd (for stylish GTKWave documents) - available on Pypi

# Installation

best done using the dev-env-setup scripts
https://git.libre-soc.org/?p=dev-env-setup.git;a=summary

Also available on pypi: pip3 install libresoc-nmutil

